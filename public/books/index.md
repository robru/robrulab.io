Date: 2017-06-05
Tags: Misc

Reading List
============

An incomplete list of books that I have greatly enjoyed in recent years and highly recommend reading.


* [Four Hour Body](http://fourhourbody.com/), by **Tim Ferriss**

    Simply put this is the only diet book that has ever worked for me. I lost 50lbs in 5 months, and I've kept it off for over a year.


* [Nonviolent Communication](https://en.wikipedia.org/wiki/Nonviolent_Communication), by **Marshall Rosenberg**

    NVC is an excellent tool for de-escalating conflict and resolving interpersonal problems quickly and effectively.


* [Five Love Languages](http://www.5lovelanguages.com/resources/books/), by **Gary Chapman**

    This is a brilliant book about the different ways that people express love for one another, and how to discover what you need in your romantic relationships in order to feel loved. A must-read if you or your romantic partner ever said "I don't feel loved."


* [How to Win Friends and Influence People](https://en.wikipedia.org/wiki/How_to_Win_Friends_and_Influence_People), by **Dale Carnegie**

    A thorough field guide on interacting positively with human beings.


* [Seven Habits of Highly Effective People](https://www.stephencovey.com/7habits/7habits.php), by **Stephen R Covey**

    If **How to Win Friends** is a guide for *behavior*, then **Seven Habits** is a guide for *thought*. This book is a step by step guide on how to develop a mindset that will improve your life.
