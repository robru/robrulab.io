Date: 2017-05-07
Tags: Health

Chronic Back Pain
=================

I suffer from chronic upper back pain, and I've tried many different products and services to alleviate it.

I've struggled with poor posture almost my entire life. When I was young, it didn't seem to matter, because I had boundless energy to recover from any problem. As I've aged, however, I've been experiencing increasing chronic back pain.

**TL;DR:** Everything that makes my shoulders go forward is bad, and everything that makes my shoulders go backward is good.

What Worked
-----------

Unfortunately there is no magic bullet. No one thing is a miracle cure for back pain. However each of these items has contributed incremental improvements, bringing my overall pain down to a manageable level.

### Physiotherapy

My physiotherapist diagnosed my problem as being a result of poor posture. Apparently a lifetime of slouching (or, "forward shoulder position" as my physiotherapist calls it) has resulted in my spine stiffening into a bad posture, and my back muscles becoming distended & weak. Physiotherapy is able to target these muscles and strengthen them, which makes them better able to hold correct posture for prolonged periods.

### Chiropractice

My chiropractor made the same diagnosis as my physiotherapist, but the treatment takes the opposite approach. Instead of strengthening muscles, chiro is about adjusting the joints into a better position so the weak muscles don't have to fight so hard against stiff joints. Both approaches complement each other very well; the result is stronger muscles and looser joints, making correct posture significantly easier.

### Yoga

I haven't yet taken a proper Yoga class with an instructor, but I plan to soon. For now, I do just a couple yoga moves recommended by my physiotherapist. Specifically, the cat-cow is highly effective at releasing tension in the shoulders. My spine will often pop as well, but not to the same degree as a session with the chiropractor. I'll typically do a set of 20 cat-cows at a time, at least once in the mornings and once before bed.

### Standing Desk

This world is seemingly full of terrible chairs that force your back into slouching positions. The popularity of terrible chairs makes me feel as though I'm the only person who ever suffered from back pain. Many chairs I've tried over the years make it actually impossible for me to sit with correct posture. After some experimentation I finally came up with a standing desk solution that I'm pleased with. One caveat is that the standing desk will shift strain from your back down into your knees, so if you have bad knees maybe skip this option.

<p class="image">Shelving Unit Desk <img alt="Shelving Unit" src="desk.jpg" /></p>

It's not even a desk, it's literally just an adjustable-height shelving unit, however it has a number of advantages over other solutions:

* Low cost
* Wall mounting takes up minimal floor space
* Doesn't collect dust, easy to clean
* Cables can be woven directly into the bars, no additional cable management necessary

### Therapy Cane

There is a fun little tool called a therapy cane. They look like a gaint fish hook. The idea is that you can grasp it in front of you and it has a big hook that reaches around and stabs you in the back, which allows you to poke and prod at your own muscles. It's no substitute for a good physiotherapy session, but it is capable of relieving some acute cramps on a day to day basis.

<p class="image">Therapy Cane <img alt="Therapy Cane" src="therapy-cane.jpg" /></p>

### New Sofa

Many sofas I've owned over the years have very soft backs, and what happens is that I'll sink right into them, but my core will sink farther than my shoulders, which effectively pushes my shoulders forward and forces poor posture. I've recently purchased a new sofa with a low, firm back, which allows my shoulders to roll back into a better position when I'm sitting.

### Car Seat Cushion

I found the pain quite bad while driving, especially long distances. "Bucket" style seats found in damn near every vehicle have a concave shape which push my shoulders forward, exacerbating my already-poor posture. I purchased a "thoracic spine cushion" (this is different than the lumbar cushions I often see marketed to relieve back pain), and now I no longer experience pain during long drives.

<p class="image">Car Seat <img alt="Car Seat" src="car-seat.jpg" /></p>

The narrow, vertical part at the top is the key thing here. It pushes the spine forward but not the shoulders, allowing the shoulders to recline farther back than the spine. This corrects posture and relieves pain.

### Posture Brace

I've recently purchased a shoulder harness / posture brace. It fits snugly around the shoulders, and pinches the pectoral muscles when posture is bad. This pinch is a constant reminder to maintain correct posture, prevents me from getting complacent and reverting to a slouch without noticing.

<a target="_blank"  href="https://www.amazon.com/gp/product/B01LYV40CI/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B01LYV40CI&linkCode=as2&tag=robru-20&linkId=3cfd29cbf0441d533b4e8bdb8789fec3"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=B01LYV40CI&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=robru-20" ></a><img src="//ir-na.amazon-adsystem.com/e/ir?t=robru-20&l=am2&o=1&a=B01LYV40CI" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />

### Foam Roller

Lying on the foam roller lengthways allows the spine to flatten out, and the shoulders roll back. This is an excellent posture corrector and there are some exercises you can do while in this position. I'd say this is probably less effective than the yoga, but is still nice and provides some variety.

<a href="https://www.amazon.com/AmazonBasics-High-Density-Round-Foam-Roller/dp/B00XM2MRGI/ref=as_li_ss_il?ie=UTF8&qid=1494172989&sr=8-3&keywords=foam+roller&linkCode=li3&tag=robru-20&linkId=47b7b328acfe153a44fa8ffbfffea059" target="_blank"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B00XM2MRGI&Format=_SL250_&ID=AsinImage&MarketPlace=US&ServiceVersion=20070822&WS=1&tag=robru-20" ></a><img src="https://ir-na.amazon-adsystem.com/e/ir?t=robru-20&l=li3&o=1&a=B00XM2MRGI" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />

What Didn't
-----------

Not everything I tried worked. Here are some items that were nice but ultimately unhelpful.

### Deep Tissue Massage / Massage Therapy

Massages feel great at the time but provide no lasting relief whatsoever. The very second I get off the massage table the pain creeps right back in.

### New Mattress

Usually my pain would be worst upon waking, leading me to believe there was something wrong with my mattress. I spent $2,000 on a new mattress which did not help at all, so don't bother with this unless you've already exhausted the less expensive options and you're really sure that the mattress you have is particularly bad. Turns out it's sleeping in the fetal position which is causing the pain and adjusting the position I sleep in has had a larger impact than replacing the mattress.

### Weed

Weed helped me sleep longer & deeper but did nothing for the pain. It also made it impossible for me to focus, even the day after, which interfered with my work. Overall not something I intend to pursue.

That said, the war on drugs is a farce that must end.

### Alcohol

Yes, the pain was so bad it drove me to drink. While fun, this generally made things worse as the dehydration causes muscle cramps which amplify the existing pain.
