Date: 2010-10-16
Tags: Python

GottenGeography
===============

GottenGeography is a tool I created to geotag photographs. It aims to be easy to use with a simple drag and drop interface. Here is a quick tutorial.


## World Map

Upon loading GottenGeography for the first time you'll be greeted with an OpenStreetMap map of the world, which you can navigate and zoom as you wish:

![Edmonton](01-edmonton.png)

## Load Photos

You can then load photos, using either the `Open` button, or drag & drop:

![Load Photos](02-load-photos.png)

## Load GPS

Once you have loaded your GPS data, GottenGeography will automatically cross-reference the timestamps in the images to the timestamps in the GPS data to place the photos on the map:

![Load GPS](03-load-gps.png)

## Adjust Clock

Sometimes your camera's clock is set incorrectly and you need to make adjustments, such as the timezone, or simply correcting drift. GottenGeography has a simple menu for this:

![Adjust Clock](04-adjust-clock.png)

When the photos appear in the locations you expect, just hit `Save All` and you're done!

## GPS Color

It's also possible to configure what color your GPS data is displayed in. This is useful if you have multiple GPS tracks loaded and they overlap.

![GPS Color](05-gps-color.png)
