Date: 2017-05-03
Tags: Misc

We Have To Go Back
==================

Overlapping windows were a mistake.

<p class="image">Xerox Star in 1981, the first system to tile windows <img alt="Xerox Star circa 1981" src="xerox-star.jpg" /></p>

Since 1981, desktop designers have been pushing the idea that windows should overlap, because that's what papers on your desk do. I can't imagine why anybody would want their computer screen to be as cluttered as the papers on their desk. This is "faster horses" level thinking.

<p class="image"><img alt="Faster Horses" src="ford-horses.png" /></p>

Just as automobiles aren't horses and behave differently than horses, computers aren't paper and they should behave differently. Better. Faster. More efficient.

Consider mobile phones. All apps run full screen, but the most powerful phones on the market can display two apps at the same time... tiled. Phones and tablets are only increasing in their popularity. The market has decided: Full screen apps with limited tiling are superior to desktops with overlapping windows.


<p class="image">LG G5 running two apps <img alt="LG G5" src="lg-g5.png" /></p>

Ubuntu Unity, GNOME Shell, and hell even Microsoft Windows all allow rudimentary tiling by dragging windows to the left and right edges of the screen. They know the writing is on the wall for overlapping windows.

So, following that train of logic, I've switched to using [i3](https://i3wm.org/) on all my laptops.

<p class="image">i3status <img alt="i3 tiling window manager" src="status.png" /></p>

When I was first configuring i3, I found the default theme to be a little bit ugly. With a little bit of patience I was able to configure it to give a more minimal, flat look, inspired somewhat by Android material design (to the extent that that's possible, I mean).

Ironically, i3's default configuration was less space-efficient than GNOME Shell, which I had configured to hide titlebars of maximized windows. i3's status bar plus titlebars actually took up more space than the GNOME Shell top bar alone. My goal was to eliminate all visual clutter, including titlebars and borders, to get the simplest possible look.

<p class="image">i3 workspace switcher <img alt="i3 workspace switcher" src="workspaces.png" /></p>

i3 doesn't actually allow you to disable the borders it draws around various elements, but it is possible to set the border color to the same color as the background color, which gives that flat look.

I'm quite pleased with what I came up with here, notice how the workspace switcher is just flat squares with no borders. Also notice how you don't need a titlebar because the window title is displayed in the browser tab. Closing or moving windows I do exclusively with the keyboard, which is faster anyway, because of the tactile feedback of the keys.

Here's a [full screenshot](i3.png) if you want to see the whole thing. If you read this far, you may want to dig into my [i3 config](https://gitlab.com/robru/dotfiles/blob/master/.config/i3/config) or even my [i3status.conf](https://gitlab.com/robru/dotfiles/blob/master/.i3status.conf).
