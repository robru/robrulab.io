Date: 2017-05-31
Tags: Health

Weight Loss
===========

I lost 50lbs thanks to this one weird book.

The only weight-loss book I've ever had any success with is [4 Hour Body by Tim Ferriss](http://fourhourbody.com/).

The reason 4HB is so effective is that it's less about prescribing a diet and more about giving you the tools you need to experiment on your own body and discover for yourself what techniques work and what don't. The two most important takeaways of the book are to track your progress daily, and photograph your meals before you eat them. The visual record of your meals makes it plain to see what foods are causing weight gain and which cause weight loss.

Weighing Yourself
-----------------

Tim prefers more advanced methods for measuring bodyfat percentages but I prefer to simply weigh myself because it's easier and cheaper. One problem is that when you measure your weight you're weighing a lot more than just your fat, you're also weighing a number of confounding factors including: your clothes, your stomach contents, your bladder contents, your bowel contents, your water weight, etc. In order to get a consistent, usable weight measurement each day, you want to do as much as possible to eliminate those factors. I've developed this morning routine in order to maximize the consistency of my weight measurements:

1. Wake up
1. Get naked
1. Use the toilet
1. Weigh yourself
1. Have breakfast

That moment after using the toilet but before eating the first meal of the day is the one point during the day where your stomach, bladder, and bowels will all be emptiest, giving you the most consistent possible measurement you can get. Do this every day, and you can track the results of your weight loss.

Your weight will naturally fluctuate throughout the day. For fun you may want to weigh yourself before and after: dressing, undressing, eating, drinking, and using the toilet. The results may surprise you! Doing this for just a couple days will make it obvious why the routine I outline above makes sense. It's easy for weight to fluctuate 2 or 3lbs throughout the day based on a number of different factors, so it really is important to control those factors to get a consistent measurement.
