Date: 2017-06-11
Tags: Misc

Thoughts on Minimalism
======================

I've considered myself a minimalist for many years now. I spend a lot of time thinking about my personal possessions and different ways that I might "minimize" them.

Corrective Optics
-----------------

Some years ago my optometrist gave me a prescription for my near sightedness. If I were to list the different options from most minimal to least minimal, it might look like this:

1. Laser eye surgery
1. Contact lenses
1. Rimless glasses
1. Regular glasses

Indeed, if the attribute you were attempting to minimize was *weight*, then this would be the correct ordering. But let's say I don't like the idea of either surgery or contact lenses touching my eyes? What if instead of weight, I wanted to minimize pain & discomfort? Then minimalism starts to look like this:

<p class="image">rimless and also hingeless<img alt="rimless glasses" src="glasses.jpg" /></p>

I wore this style of glasses for a couple years thinking they were as minimalist as you could get without having to physically touch your eyes. The lenses were acrylic which made them so light (compared to glass lenses) that I could barely feel the weight of these things on my face. Often I would forget I even had them on.

But there was a problem. The lack of hinges in the arms made it very difficult to fold these up and hang them off my shirt collar. So when I didn't need them they were rather impractical to store and carry. Worse, the nose pads started falling off at the worst possible times (is there a good time for your glasses nose pads to fall off?). Within the context of minimizing discomfort, I realized this aim would be served best by some acrylic frames where the nose pads were all one piece and couldn't possibly fall off. I then arrived at this:

<p class="image">totally ordinary glasses<img alt="ordinary glasses" src="minimal-glasses.jpg" /></p>

You probably wouldn't look at those glasses and think "those are a minimalist's glasses" and yet I came to this conclusion by minimizing discomfort, impracticality, and failure modes.

Sofas
-----

Now I'll make the case that my 3-piece sectional sofa that seats 7 people is more minimal than the previous sofa that was just one section and seated only 3 people. ;-)

I used to have a totally ordinary IKEA sofa. It wasn't particularly comfortable but it was cheap (minimizing cost!) and worked fine. One problem I had with it was that it didn't fit on my elevator, which annoyed me a lot more than it should have given how infrequently I would ever move it. Getting it on the elevator required dismantling it, which required a wrench to undo 8 bolts, so that it could be split into 4 pieces.

By replacing it with a sectional sofa, I have minimized the amount of work required to get the sofa into the elevator, because the three sections of the sofa all fit into the elevator without issue individually.

Also, by having a larger sofa, I've minimized the discomfort of guests who visit when I host parties.

Conclusion
----------

I find minimalism really interesting because minimizing certain attributes of a given item can maximize other attributes. There's a lot of nuance and reasonable people can disagree about which things are more or less minimal than other things. It depends on what aspect of the item you are trying to minimize.
