Date: 2017-05-11
Tags: Web

Creating a Responsive HTML Résumé
=================================

I'm back on the job market again, and that means one thing: Creating a professional résumé.

<p class="image">Three column résumé design <img alt="Wide Screen, Three Columns" src="wide.png" /></p>

If you've ever applied for a job, you've probably had this experience: You're typing up the perfect résumé in your favorite word processor, you spend hours agonizing over the perfect font choice, styling, etc. Then you export it to a very professional looking PDF! But when it comes time to submit your job application online, all they have is a text box. When you copy your résumé in, it loses all of it's formatting and generally looks like junk.

For a number of years, I've used Michael White's solution to this problem, which is to write your résumé [in Markdown](http://mwhite.github.io/resume/). He then converts the Markdown to HTML and PDF. This approach has many benefits: You keep one master copy of your résumé stored in a simple text format, the conversion process is easy and produces professional results, and when you hit that dreaded textbox résumé form, you can copy in the raw markdown and not be embarrassed by the result.

In spite of all those benefits, I felt like it was time for something new. I had been inspired by seeing a friend do a multi-column résumé layout, and I was feeling creative, so I decided to see what I could come up with on my own.

My first thought was that I would start with a format that was more structured than Markdown, so that I could specify more formatting like columns and pagination. I began investigating LateX, but the more I learned about LaTeX the more I thought to myself: If I'm going to write in a markup language, why not just write HTML?

Once I had decided on an HTML résumé, it became obvious that I would need to show off my responsive design skills: It would be a three-column layout that gracefully squeezes down to two, and ultimately one column depending on the width of your screen.

<p class="image">On smaller screens, the third column moves down to the bottom of the second column <img alt="Medium Width, Two Columns" src="two.png" /></p>

<p class="image">On mobile, all three columns align into a single column <img alt="Mobile, One Column" src="mobile.png" /></p>

<p class="image">If printed, the columns line up nicely with the page breaks <img alt="Printed Pagination" src="paginated.png" /></p>

This is all served from a single HTML file! And when I do come across one of those dreaded text résumé input boxes, I can render the HTML into text with lynx and it [looks ok too](/RobertPark.txt).

This is achieved with a relatively simple arrangement of `<div>` tags and css. Start with the HTML:

    <div id="contact">photo, email, etc</div>
    <div id="body">
    <div class="page">first page...</div>
    <div class="page">second page...</div>
    </div>

Start your CSS with universal settings, such as choosing fonts & colors to be shared by all display types. Then, add the following to the end of your CSS file to make the two-column layout function:

    @media only screen and (min-width: 800px) {
        #contact {
            display: inline-block;
            vertical-align: top;
        }
        #body {
            display: inline-block;
            vertical-align: top;
        }
        .page {
            max-width: 500px;
        }
    }

And this bit makes the third column appear:

    @media only screen and (min-width: 1400px) {
        .page {
            display: inline-block;
            vertical-align: top;
        }
    }

And finally this bit forces pagination for printing and PDFs:

    @media print {
        #contact, #body, .page {
            display: block;
        }
        .page:not(:last-child) {
            page-break-after: always;
        }
    }

Do [check out the finished product](/RobertPark.html) and try resizing your browser window, you can see it respond dynamically to fill the available space.
